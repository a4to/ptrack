ascii_magic==2.3.0
beautifulsoup4==4.12.2
Pillow==10.1.0
Requests==2.31.0
rich==13.7.0
yt_dlp==2023.10.13
humanize==4.9.0
validators==0.22.0
